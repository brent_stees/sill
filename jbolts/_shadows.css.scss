/* Shadow */

// $offset supports southeast or northwest (-n)
@mixin shadow($offest: 0.5em,$blur: 0.5em,$color: #222,$inset : '') {
  -moz-box-shadow: $offest $offest $blur $color #{$inset};
  -webkit-box-shadow: $offest $offest $blur $color #{$inset};
  box-shadow: $offest $offest $blur $color #{$inset};
}

// Used to make a double inset
@mixin inset($offest: 1px,$blur: 0, $light: rgba(255,255,255,0.8), $dark: rgba(0,0,0,0.8)) {
  -moz-box-shadow: $offset $offest $blur $dark inset, -#{$offset} -#{$offset} $blur $light inset;
  -webkit-box-shadow: $offset $offest $blur $dark inset, -#{$offset} -#{$offset} $blur $light inset;
  box-shadow: $offset $offest $blur $dark inset, -#{$offset} -#{$offset} $blur $light inset;
}

%shadow_small { @include shadow(rgba(0,0,0,0.3),0.2em,0.2em); }
%shadow_large { @include shadow(rgba(0,0,0,0.3),0.6em,0.6em); }
%shadow_inset { @include shadow(rgba(0,0,0,0.3),0.08em,0.1em,inset); }

// Used to make a drop shadow under text
@mixin text_shadow($h: 1px, $v: 1px, $b: 1px, $c: rgba(0,0,0,0.8)) {
  $value: $h $v $b $c;
  -moz-text-shadow: $value;
  -webkit-text-shadow: $value;
  -o-text-shadow: $value;
  text-shadow: $value;
}

%text_shadow { @include text_shadow(); }

// Used to make text look inset
@mixin text-inset($h: 1px, $v: 1px, $b: 1px, $dark: rgba(0,0,0,0.8), $light: rgba(255,255,255,0.8)) {
  $value: -#{$h} -#{$v} $b $dark, $h $v $b $light;
  -moz-text-shadow: $value;
  -webkit-text-shadow: $value;
  -o-text-shadow: $value;
  text-shadow: $value;
}

%text_inset { @include text-inset(); }